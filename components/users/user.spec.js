const assert = require('assert'),
      { User } = require('./index')

describe('User module', () => {

    it('Cleanup documents', async () => {
        const err = await User.deleteMany({})
        const results = await User.load()
        assert.strictEqual(results.length, 0)
    })
    
})