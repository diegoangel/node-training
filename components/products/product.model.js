const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Enter title']
    },
    price: {
        type: Number,
        required: true,
        min: [0, 'Need to be a positive value']
    },
    stock: {
        type: Number,
        required: true,
        min: [0, 'Need to be a positive value']
    },
    description: String
})

schema.pre('save', next => {
    console.log(`Product has saved`)
    next()
})

module.exports = mongoose.model('Product', schema)