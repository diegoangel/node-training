const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    isShipped: Boolean,
    isCancelled: Boolean,
    total: {
        type: Number,
        required: true,
        min: [0, 'Need to be a positive value']
    },
    items: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    comments: [{ message: String, created_at: Date }]
})

schema.pre('save', next => {
    console.log(`Order has saved`)
    next()
})

module.exports = mongoose.model('Order', schema)