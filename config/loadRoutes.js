const glob = require('glob')

const globOptions = {
    cwd: __dirname
}

module.exports = app => {
    return new Promise(resolve => {
        glob('../**/*.route.js', globOptions, (err, files) => {

            if (err)
                throw new Error(err)
            else if (!files.length)
                console.log('No route found')
            
            files.forEach(filename => {
                const routeName = /\/([A-Za-z]*)\.route/s.exec(filename)[1]
                app.use(`/${routeName}`, require(filename))
            })
            
            resolve()
        })
    })
}