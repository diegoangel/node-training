const requiredVariables = ['NODE_ENV']

requiredVariables.forEach((name) => {
    if (!process.env[name]) {
        console.error(`Environment variable ${name} is missing`)
        process.exit()
    }
})

const config = {
    env: process.env.NODE_ENV,
    logger: {
        level: process.env.LOG_LEVEL || 'common',
        enabled: process.env.BOOLEAN ? process.env.BOOLEAN.toLowerCase() === 'true' : false
    },
    server: {
        port: Number(process.env.PORT || '3000')
    },
    mongo: {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || '27017',
        db: process.env.DB_NAME || 'test'
    }
}

module.exports = config