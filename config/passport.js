const passport = require('passport'),
      BearerStrategy = require('passport-http-bearer').Strategy,
      { User } = require('../components/users/index')

passport.use(new BearerStrategy(async (token, done) => {
    
    try {
        const user = await User.load(token)
        return done(null, user, { scope: 'all' })
    } catch (err) {
        return done(null, false)
    }   

}))