const assert = require('assert'),
      request = require('supertest'),
      server = require('../index'),
      req = request(server)

describe('User endpoints', () => {

    describe('POST /user', () => {
        it('Create an user', (done) => {

            const name = 'Ada Lovelace'

            req
            .post('/user')
            .send({"name": name, "age": 30, "genre": "Female"})
            .set('Content-Type', 'application/json')
            .expect(200)
            .expect(res => {
                assert.ok(typeof res.body === 'object', 'The response is not an Object')
                assert.strictEqual(res.body.name, name, 'The operation failed')
            })
            .end(done)

        })
    })

    describe('GET /user', () => {
        it('Retrieves all users', (done) => {
            done()
        })
    })

    describe('GET /user/userId', () => {
        it('Retrieves an user object', (done) => {
            done()
        })
    })

    describe('PUT /user/userId', () => {
        it('Updates user by ID', (done) => {
            done()
        })
    })

    describe('DELETE /user/userId', () => {
        it('Delete user by ID', (done) => {
            done()
        })
    })
})